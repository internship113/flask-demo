#import library
import  logging

levelToName = {
    50:"fatal",
    40:"error",
    30:"warn",
    20:"info",
    10:"debug"
}

logLevels = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warn':logging.ERROR,
    'fatal':logging.FATAL
}

def set_log_level(level):
    log_level = logLevels[level]
    logging.basicConfig(level=log_level,format='time="%(asctime)s" level=%(levelname)s msg="%(message)s"')

def set_format():
    logging.basicConfig(format='time="%(asctime)s" level=%(levelname)s msg="%(message)s"')

def debug(message):
    logging.debug(message)

def info(message):
    logging.info(message)

def warning(message):
    logging.warning(message)

def error(message):
    logging.error(message)

def critical(message):
    logging.critical(message)


