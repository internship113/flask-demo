#import lib
import time
from flask import Flask, request as flask_request
from flask import redirect,url_for,request
import requests
import json
from werkzeug.exceptions import HTTPException
from pytube import YouTube
import os

import log

# set log level
log.set_log_level("info")

# Initalization API config
BATCH_SIZE = 64
WORKER = 0

app = Flask(__name__)
RESTful_version = "v1"


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    response = e.get_response()
    response.data = json.dumps({
        "error": {
            "code": e.code,
            "error_name": e.name,
            "message": e.description
        }
    })
    response.content_type = "application/json"
    return response


def handle_exception(e, code):
    """Return JSON instead of HTML for HTTP errors."""
    response = json.dumps({
        "error": {
            "code": code,
            "message": str(e)
        }
    })
    return response, code, {'Content-Type': 'application/json'}


@app.route('/{}/'.format(RESTful_version), methods=['GET'])
def hello():
    return 'hello', 200, {'Content-Type': 'text'}

# Varible rule


@app.route('/{}/<string:name>'.format(RESTful_version, methods=['GET']))
def hello_name(name):
    return 'hello %s!' % name


@app.route('/{}/<int:num>'.format(RESTful_version), methods=['GET'])
def hello_num(num):
    return 'Num = %d !' % num


@app.route('/{}/<float:double>'.format(RESTful_version), methods=['GET'])
def hello_float(double):
    return 'float = %f !' % double

# URL building
@app.route('/{}/admin'.format(RESTful_version), methods=['GET'])
def hello_admin():
    return 'hello admin', 200, {'Content-Type': 'text'}


@app.route('/{}/guest/<guest>'.format(RESTful_version), methods=['GET'])
def hello_guest(guest):
    return 'hello %s as Guest' % guest


@app.route('/{}/user/<name>'.format(RESTful_version), methods=['GET'])
def hello_user(name):
    if name == 'admin':
        return redirect(url_for('hello_admin'))
    else:
        return redirect(url_for('hello_guest', guest=name))

# HTTP methods
@app.route('/{}/success/<name>'.format(RESTful_version))
def success(name):
    return 'welcome %s' % name


@app.route('/{}/login'.format(RESTful_version), methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        user = request.form['name']
        return redirect(url_for('success', name=user))
    else:
        user = request.args.get('nm')
        return redirect(url_for('success', name=user))

# Health check
@app.route('/{}/heathcheck'.format(RESTful_version), methods=['GET'])
def healthcheck():
    return 'Welcome to flask-demo API Test'

# Get youtube channel stats
@app.route('/{}/youtube-stat/<channeld>'.format(RESTful_version), methods=['GET'])
def getData(channeld):
    url = f'https://www.googleapis.com/youtube/v3/channels?part=statistics&id={channeld}&key=AIzaSyBOZVTfa0ktKbvvpmm2riCFRroXcw0dxKs'

    json_url = requests.get(url)
    data = json.loads(json_url.text)
    print(data)
    return data, 200, {'Content-Type': 'application/json'}

# Download video


@app.route('/{}/youtube/download/'.format(RESTful_version, methods=['GET']))
def downloadYouTube():
    print('download.....')
    try:
        required_parameter = ['video_link']
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception("Request parameter '{}' not found".format(param))
        videourl = flask_request.form['video_link']
        print(videourl)
        path = '/home/ming/Workspace/flask-demo/download'
        yt = YouTube(videourl)
        yt = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first()
        if not os.path.exists(path):
            os.makedirs(path)
        yt.download(path)
        return 'video downloaded file location : %s ' % path , 200 ,{'Content-Type':'text'}
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message,code=400)

#GET 
@app.route('/{}/getfile/'.format(RESTful_version),methods = ['GET'])
def get_file():
    try:
        filepath = []
        required_parameter = ['filepath']
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception("Request parameter '{}' not found".format(param))
        filepath = flask_request.form.getlist('filepath')
        print('Quantity : ',len(filepath))
        print(filepath)
        file=[]
        for i in range(0,len(filepath)):
            details = {
                "fileName": os.path.splitext(os.path.basename(filepath[i]))[0],
               "fileExtendsion": os.path.splitext(os.path.basename(filepath[i]))[1], 
            }
            file.append(details)
        print(file)
        return  str(file) , 200
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message,code=400)

#POST
@app.route('/{}/createfile/'.format(RESTful_version),methods = ['POST'])
def create_file():
    try:
        required_parameter = ['filepath']
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception("Request parameter '{}' not found".format(param))
        filepath = flask_request.form['filepath']
        file = open(filepath,"w")
        file.close()
        return  'file created at %s !' % filepath , 201 ,{'Content-Type': 'text'}
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message,code=400)

#PUT
@app.route('/{}/editfile/'.format(RESTful_version),methods = ['PUT'])
def edit_file_name():
    try:
        required_parameter = ['filepath','filename']
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception("Request parameter '{}' not found".format(param))
        filepath = flask_request.form['filepath']
        filename = flask_request.form['filename']
        os.rename(filepath,os.path.split(filepath)[0]+'/'+filename)
        return  'file renamed at %s !' % os.path.split(filepath)[0]+'/'+filename , 200
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message,code=400)

#DELETE
@app.route('/{}/deletefile/'.format(RESTful_version),methods = ['DELETE'])
def delete_file():
    try:
        required_parameter = ['filepath']
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception("Request parameter '{}' not found".format(param))
        filepath = flask_request.form['filepath']
        os.remove(filepath)
        return  'file renamed has been deleted' ,200 , {'Content-Type': 'text'}
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message,code=400)

log.info('READY!!!')
if __name__ == '__main__':
    app.debug = True
    app.run(host='127.0.0.1', port='8080', debug=True)
